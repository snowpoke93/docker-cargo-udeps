FROM rustlang/rust:nightly

# Install cargo-udeps from Github release
RUN ARCH=$(uname -m) && \
    export LATEST_UDEPS_VERSION=$(git ls-remote --tags --sort="v:refname" --refs https://github.com/est31/cargo-udeps.git | tail -n1 | awk '{print $2}' | awk -F '/' '{print $3}'); \
    curl -LJO https://github.com/est31/cargo-udeps/releases/download/${LATEST_UDEPS_VERSION}/cargo-udeps-${LATEST_UDEPS_VERSION}-${ARCH}-unknown-linux-gnu.tar.gz; \
    tar -xzf cargo-udeps-${LATEST_UDEPS_VERSION}-${ARCH}-unknown-linux-gnu.tar.gz; \
    mv cargo-udeps-${LATEST_UDEPS_VERSION}-${ARCH}-unknown-linux-gnu/cargo-udeps ./bin/;

# move target dir out of project folder
RUN mkdir /target
ENV CARGO_TARGET_DIR=/target

WORKDIR /project

CMD ["cargo", "udeps", "--quiet"]
